# Changelog

[![Chrome Web Store](https://img.shields.io/chrome-web-store/v/lfddnkicgngldglbjdiammicccpangcb.svg)](https://chrome.google.com/webstore/detail/comedy-corner-dilbert/lfddnkicgngldglbjdiammicccpangcb) [![Chrome Web Store](https://img.shields.io/chrome-web-store/users/lfddnkicgngldglbjdiammicccpangcb.svg)](https://chrome.google.com/webstore/detail/comedy-corner-dilbert/lfddnkicgngldglbjdiammicccpangcb) [![Chrome Web Store](https://img.shields.io/chrome-web-store/rating/lfddnkicgngldglbjdiammicccpangcb.svg)](https://chrome.google.com/webstore/detail/comedy-corner-dilbert/lfddnkicgngldglbjdiammicccpangcb)  [![License](https://img.shields.io/badge/license-GPL--2.0%2B-red.svg)](https://github.com/evertiro/comedy-corner-dilbert/blob/master/license.txt)

## Version 1.0.5

* Fixed: URL mapping

## Version 1.0.4

* Added: Option to disable the 'save last viewed' feature

## Version 1.0.3

* Added: Proper error handling
* Improved: Loading animation
* Fixed: Next/previous link visibility on most recent/oldest comics

## Version 1.0.2

* Added: Search support

## Version 1.0.1

* Fixed: Product URL

## Version 1.0.0

* Added: Initial release