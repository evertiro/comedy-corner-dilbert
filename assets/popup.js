jQuery(document.body).ready(function ($) {
    'use strict';

    var no_last_viewed, comic_date, comic_date_hr, current_date, comic_url, setup;
    var min_date     = 608706000000;
    var timezone     = 'America/Chicago';
    var base_url     = 'https://dilbert.com/strip/';

    /**
     * Setup all the things!
     */
    setup = {
        init : function () {
            // Load settings
            no_last_viewed = localStorage.getItem( 'ccdilbert-no-last-viewed' );

            this.getComicDate( no_last_viewed );
            this.handleSettings( no_last_viewed );
            this.handleLinks();
            this.handleDate();
            this.getComic();
        },
        getComicDate : function ( no_last_viewed ) {
            current_date = moment().valueOf();

            // Check if we have a last viewed comic
            comic_date = localStorage.getItem( 'ccdilbert-last-viewed' );

            if ( ! comic_date || no_last_viewed === 'true' ) {
                comic_date = current_date;
            }

            comic_date    = parseInt( comic_date );
            comic_date_hr = moment( comic_date ).format( 'YYYY-MM-DD' );
            comic_url     = base_url + comic_date_hr;

            $( '.comic-date-form' ).val( comic_date_hr );
        },
        handleSettings : function ( no_last_viewed ) {
            if ( no_last_viewed === 'true' ) {
                $( '.setting-checkbox' ).addClass( 'active' );
                $( '.setting-checkbox i' ).removeClass( 'fa-square' );
                $( '.setting-checkbox i' ).addClass( 'fa-check-square' );
            }

            // Settings link
            $( '.settings-toggle' ).click( function () {
                if ( $( '.settings' ).is( ':visible' ) ) {
                    $( '.settings' ).fadeOut( 'fast', function() {
                        // Reload comic
                        no_last_viewed = localStorage.getItem( 'ccdilbert-no-last-viewed' );

                        setup.getComicDate( no_last_viewed );
                        setup.getComic();

                        $( '.frame' ).fadeIn( 'fast' );
                    } );
                    
                } else {
                    $( '.frame' ).fadeOut( 'fast', function() {
                        $( '.settings' ).fadeIn( 'fast' );
                    } );
                }
            } );

            // Eventually, I'll end up with multiple settings and this will have to be rewritten
            $( '.setting-checkbox' ).click( function () {
                var checkbox = $( this ).find( 'i' );

                if ( $( this ).hasClass( 'active' ) ) {
                    $( this ).removeClass( 'active' );
                    $( checkbox ).removeClass( 'fa-check-square' );
                    $( checkbox ).addClass( 'fa-square' );
                    localStorage.setItem( 'ccdilbert-no-last-viewed', 'false' );
                } else {
                    $( this ).addClass( 'active' );
                    $( checkbox ).removeClass( 'fa-square' );
                    $( checkbox ).addClass( 'fa-check-square' );
                    localStorage.setItem( 'ccdilbert-no-last-viewed', 'true' );
                }
            } );
        },
        handleLinks : function () {
            // Comic links
            $( '.comic-img, .comic-title' ).click( function () {
                window.open( comic_url );
            } );

            // Next button
            $('.next').click( function () {
                $( '.loading' ).css( 'display', 'block' );

                comic_date    = moment( comic_date ).add( 1, 'days' );
                comic_date_hr = moment( comic_date ).format( 'YYYY-MM-DD' );
                comic_url     = base_url + comic_date_hr;

                $( '.comic-date-form' ).val( comic_date_hr );

                setup.getComic();
            } );

            // Previous button
            $('.prev').click( function () {
                $( '.loading' ).css( 'display', 'block' );

                comic_date    = moment( comic_date ).subtract( 1, 'days' );
                comic_date_hr = moment( comic_date ).format( 'YYYY-MM-DD' );
                comic_url     = base_url + comic_date_hr;

                $( '.comic-date-form' ).val( comic_date_hr );

                setup.getComic();
            } );

            // Today button
            $('.today').click( function () {
                $( '.loading' ).css( 'display', 'block' );

                comic_date    = moment().valueOf();
                comic_date_hr = moment( comic_date ).format( 'YYYY-MM-DD' );
                comic_url     = base_url + comic_date_hr;

                $( '.comic-date-form' ).val( comic_date_hr );

                setup.getComic();
            } );
            
            // Random button
            $('.rand').click( function () {
                $( '.loading' ).css( 'display', 'block' );

                comic_date    = Math.floor( Math.random() * ( current_date - min_date + 1 ) + min_date );
                comic_date_hr = moment( comic_date ).format( 'YYYY-MM-DD' );
                comic_url     = base_url + comic_date_hr;

                $( '.comic-date-form' ).val( comic_date_hr );

                setup.getComic();
            } );

            // Search form
            $( '.search' ).submit( function (e) {
                e.preventDefault();

                var search_url = $( '.search :input' ).val();
                window.open( 'https://dilbert.com/search_results?terms=' + search_url );
            } );
        },
        handleDate : function () {
            $( '.comic-date-form' ).change( function () {
                comic_date_hr = $(this).val();
                comic_date    = moment( comic_date_hr ).valueOf();
                comic_url     = base_url + comic_date_hr;

                setup.getComic();
            } );
        },
        getComic : function () {
            $.ajax( {
                type : 'GET',
                url : comic_url,
                success : function ( response ) {
                    if ( response !== null ) {
                        var url = $( '.img-comic-link', response ).prop( 'href' );

                        if ( url != comic_url ) {
                            setup.showError();
                            return;
                        }

                        setup.showComic( response, url );
                    } else {
                        if ( window.console && window.console.log ) {
                            setup.showError();
                        }
                    }

                    return;
                }
            } ).fail( function ( response ) {
                if ( window.console && window.console.log ) {
                    setup.showError();
                }
            } );
        },
        showComic : function ( response, url ) {
            var img   = $( '.img-comic', response ).prop( 'src' );
            var title = $( '.comic-title-name', response ).text();

            // Dilbert no longer uses absolute URLs
            img = img.replace( 'chrome-extension', 'https' );

            // Store last viewed
            localStorage.setItem( 'ccdilbert-last-viewed', moment( comic_date ).valueOf() );

            $( '.frame' ).css( 'display', 'block' );
            $( '.loading' ).css( 'display', 'none' );

            // Setup title
            $( '.comic-title' ).html( moment( comic_date ).format( 'dddd MMMM DD, YYYY' ) + '<small>' + title + '</small>' );

            // Setup image
            $( '.comic-img' ).prop( 'src', img );
            $( '.comic-img' ).prop( 'alt', title );

            // Maybe hide nav buttons?
            if ( moment( comic_date ).format( 'YYYY-MM-DD' ) === moment( current_date ).format( 'YYYY-MM-DD' ) ) {
                $( '.next' ).css( 'visibility', 'hidden' );
            } else {
                $( '.next' ).css( 'visibility', 'visible' );
            }
            
            if ( moment( comic_date ).format( 'YYYY-MM-DD' ) === moment( min_date ).format( 'YYYY-MM-DD' ) ) {
                $( '.prev' ).css( 'visibility', 'hidden' );
            } else {
                $( '.prev' ).css( 'visibility', 'visible' );
            }
        },
        showError : function () {
            $( '.frame' ).css( 'display', 'none' );
            $( '.loading' ).css( 'display', 'none' );
            $( 'body' ).addClass( 'errorpage' );
            $( '.error' ).css( 'display', 'block' );
        }
    };
    setup.init();
} );

function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}